package com.special.ResideMenu;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ResideMenuItem extends LinearLayout {

    private ImageView ivIcon;
    private TextView tvTitle;
    private TextView tvEmail;

    public ResideMenuItem(Context context) {
        super(context);
        initViews(context);
    }

    public ResideMenuItem(Context context, int icon, int title) {
        super(context);
        initViews(context);
        ivIcon.setImageResource(icon);
        tvTitle.setText(title);
    }

    public ResideMenuItem(Context context, int icon, String title) {
        super(context);
        initViews(context);
        ivIcon.setImageResource(icon);
        tvTitle.setText(title);
    }

    private void initViews(Context context) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.residemenu_item, this);
        ivIcon = (ImageView) findViewById(R.id.iv_icon);
        tvTitle = (TextView) findViewById(R.id.tv_title);
        tvEmail = (TextView) findViewById(R.id.tv_email);
    }

    /**
     * set the icon color;
     *
     * @param icon
     */
    public void setIcon(int icon) {
        ivIcon.setImageResource(icon);
    }

    public void setIcon(Bitmap bitmap) {
        ivIcon.setImageBitmap(bitmap);
    }

    /**
     * set the title with resource
     * ;
     *
     * @param title
     */
    public void setTitle(int title) {
        tvTitle.setText(title);
    }

    /**
     * set the title with string;
     *
     * @param title
     */
    public void setTitle(String title) {
        tvTitle.setText(title);
    }

    public TextView getTitle() {
        return tvTitle;
    }

    public void setEmail(String email) {
        tvEmail.setText(email);
    }

    public TextView getEmail() {
        return tvEmail;
    }

    public ImageView getIcon() {
        return ivIcon;
    }
}

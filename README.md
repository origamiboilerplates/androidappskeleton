# Android App Skeleton

Android app skeleton is build to start any new app with already configures commonly used libraries and network layer integration.


## Step 1

To start new android app just clone the android app skeleton project from following link. https://bitbucket.org/origamiboilerplates/androidappskeleton


### Base URL

Add yout api base URL in constants file

```
\app\src\main\java\com\adc\origamistudios\utilities\Constants.java

//add your api URL
String kBaseUrl = "https://your_api_url/";

```

## Step 2

Add your api route for login,  add parameter as your api require and request type, GET, POST, PUT, DELETE

```
\app\src\main\java\com\adc\origamistudios\network\ApiInterface.java

// route name like  (login)
@FormUrlEncoded
@POST("login")
Call<LoginResponse> getLoginResponse(@Field("username") String username,
                                     @Field("password") String password,
                                     @Field("device_type") String deviceType,
                                     @Field("device_token") String deviceToken);
```

## Step 3

Call your api interface method from Login, fragment or Activity

```
   Call<LoginResponse> call = AppController.getApiService().getLoginResponse("test12345678", "test12345678", "abcdefghi", "token string");
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
               //success response
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
              //failure response
            }
        });
```

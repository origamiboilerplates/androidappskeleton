package com.adc.origamistudios.model.apiresponse;

import com.google.gson.annotations.SerializedName;

public class ApiResponse {

    @SerializedName("responseCode")
    private int responseCode;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private Object data;

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}

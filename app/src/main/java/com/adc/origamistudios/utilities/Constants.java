package com.adc.origamistudios.utilities;

public interface Constants {
    int minTime = 1000;
    int maxTime = 9000;
    int SPLASH_DELAY_TIME = 3000;

    String mFIELD = "fields";
    String mPARAMETERS = "id,name,email,gender,birthday,picture.type(large),cover,age_range";

    // Add your API base URL
    String kBaseUrl = "https://your_api_url/"; // API URL

    /////////////////////////////////Toast & Alert Messages

    String kStringNetworkConnectivityError = "Please make sure your device is connected with internet.";
    String kStringTitlePermission = "Permission necessary";
    String kStringExternalStoragePermission = "External storage permission is necessary";


    //////////////////////////Shared Preferences
    String PREF_IS_SKIP = "onBoardSkip";
    String PREF_IS_USER_LOGED_IN = "isUserLogedIn";
    String PREF_USER_ACCESS_TOKEN = "access_token";
    String PREF_NOTIFICATION_SETTING = "notifactionSetting";

    int REQUEST_101 = 101;



    //////////////////////////Arguments
    String mBearer = "Bearer ";
    String mDeviceType = "Android";
    String mGender = "GENDER";
    String EXCEPTION = "Exception";

    //////////////////////////Extras
    String FRAG_POSITION = "value";

    String EMAIL = "email";
    String CODE = "code";
    String TITLE = "title";

    // Save response
    String USER_DATA = "userData";

}

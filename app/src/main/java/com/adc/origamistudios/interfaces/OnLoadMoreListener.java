package com.adc.origamistudios.interfaces;

public interface OnLoadMoreListener {
    void onLoadMore();
}
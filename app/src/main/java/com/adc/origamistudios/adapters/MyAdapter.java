package com.adc.origamistudios.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.adc.origamistudios.utilities.GlideRequestOptions;
import com.adc.origamistudios.R;
import com.adc.origamistudios.interfaces.OnLoadMoreListener;
import com.adc.origamistudios.model.appmodels.Products;
import com.adc.origamistudios.utilities.Util;
import com.adc.origamistudios.widgets.CustomTextView;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;


public class MyAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements View.OnClickListener {
    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    private OnLoadMoreListener onLoadMoreListener;
    private boolean isLoading;
    private Activity activity;
    private List<Products> list;
    private List<Products> dataList;
    private final int visibleThreshold = 1;
    private int lastVisibleItem, totalItemCount;
    private final ClickListener itemClickListener;
    private final Context mContext;

    public MyAdapter(RecyclerView recyclerView, List<Products> list, Context context, ClickListener listener) {
        this.list = list;
        this.mContext = context;
        this.itemClickListener = listener;
        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    if (onLoadMoreListener != null) {
                        onLoadMoreListener.onLoadMore();
                    }
                    isLoading = true;
                }
            }
        });
    }

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.onLoadMoreListener = mOnLoadMoreListener;
    }

    @Override
    public int getItemViewType(int position) {
        return list.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            ViewHolder holder;
            View view = LayoutInflater.from(mContext).inflate(R.layout.item_view_brands, parent, false);
            holder = new ViewHolder(view);
            view.setTag(holder);
            holder.mainItemView.setOnClickListener(this);
            return holder;
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.item_loading, parent, false);
            return new LoadingViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolder) {
            Products item = list.get(position);
            ViewHolder userViewHolder = (ViewHolder) holder;
            userViewHolder.tvTitle.setText(item.getProductName());
            userViewHolder.tvDescription.setText(item.getLongDescription());
            try {
                if (!item.getImage().equalsIgnoreCase("") && Util.isValidURL(item.getImage())) {
                    Glide.with(mContext).load(item.getImage()).apply(GlideRequestOptions.brandsImageOptions).into(userViewHolder.ivImage);
                }
            } catch (Exception e) {
                Log.e("Error", e.toString());
            }
            userViewHolder.mainItemView.setTag(userViewHolder);
        } else if (holder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemCount() {
        return null != list ? list.size() : 0;
    }


    public void setLoaded() {
        isLoading = false;
    }

    private class LoadingViewHolder extends RecyclerView.ViewHolder {
        public final ProgressBar progressBar;

        public LoadingViewHolder(View view) {
            super(view);
            progressBar = view.findViewById(R.id.progressBar1);
        }
    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        public final CustomTextView tvTitle;
        public final CustomTextView tvDescription;
        public final ImageView ivImage;
        public final View mainItemView;

        public ViewHolder(View view) {
            super(view);
            mainItemView = view;
            tvTitle = view.findViewById(R.id.txt_name);
            tvDescription = view.findViewById(R.id.tvDescription);
            ivImage = view.findViewById(R.id.iv_img);
        }
    }

    @Override
    public void onClick(View v) {
        ViewHolder holder = (ViewHolder) v.getTag();
        int position = holder.getAdapterPosition();

        if (holder.mainItemView == v) {
            itemClickListener.onItemClicked(position, list.get(position));
        }
    }

    public interface ClickListener {
        void onItemClicked(int position, Products item);

        void onItemLongClicked(int position, Products item);
    }

    public void setFilter(List<Products> filteredBrands) {
        list = new ArrayList<>();
        list.addAll(filteredBrands);
        notifyDataSetChanged();
    }

    public List<Products> filterData(String query) {
        List<Products> filteredList = new ArrayList<>();
        if (dataList == null)
            dataList = list;

        for (Products item : dataList) {
            if (Util.containsIgnoreCase(item.getProductName().toLowerCase(), query.toLowerCase())) {
                filteredList.add(item);
            }
        }
        return filteredList;
    }

}
package com.adc.origamistudios.network;

import com.adc.origamistudios.model.apiresponse.LoginResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ApiInterface {

    @FormUrlEncoded
    @POST("register")
    Call<LoginResponse> getRegistrationResponse(@Field("first_name") String firstName,
                                                @Field("last_name") String lastName,
                                                @Field("username") String username,
                                                @Field("password") String password,
                                                @Field("password_confirmation") String passwordConfirmation,
                                                @Field("gender") String gender,
                                                @Field("email") String email,
                                                @Field("device_type") String deviceType,
                                                @Field("device_token") String deviceToken);

    @FormUrlEncoded
    @POST("login")
    Call<LoginResponse> getLoginResponse(@Field("username") String username,
                                         @Field("password") String password,
                                         @Field("device_type") String deviceType,
                                         @Field("device_token") String deviceToken);

    @FormUrlEncoded
    @POST("login/social")
    Call<LoginResponse> getFacebookLoginResponse(@Field("fb_id") String fbId,
                                                 @Field("first_name") String firstName,
                                                 @Field("last_name") String lastName,
                                                 @Field("email") String email,
                                                 @Field("gender") String gender,
                                                 @Field("avatar") String url,
                                                 @Field("device_type") String deviceType,
                                                 @Field("device_token") String deviceToken);

}

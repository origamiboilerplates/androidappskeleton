package com.adc.origamistudios.fcm;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.adc.origamistudios.R;
import com.adc.origamistudios.presentation.activities.ResideMenuActivity;
import com.adc.origamistudios.utilities.Constants;
import com.adc.origamistudios.utilities.Utilities;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;


public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "VoiceFCMService";
    private static final String NOTIFICATION_ID_KEY = "NOTIFICATION_ID";
    private static final String CALL_SID_KEY = "CALL_SID";


    @Override
    public void onCreate() {
        super.onCreate();
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
    }

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.d(TAG, "Received onMessageReceived()");
        Log.d(TAG, "Received onMessageReceived(): " + remoteMessage);
        Log.d(TAG, "Bundle data: " + remoteMessage.getData());
        Log.d(TAG, "From: " + remoteMessage.getFrom());


        if (!Utilities.getInstance(getApplicationContext()).getBooleanPreferences(Constants.PREF_IS_USER_LOGED_IN)) {
            return;
        }
        if (Utilities.getInstance(getApplicationContext()).getBooleanPreferences(Constants.PREF_NOTIFICATION_SETTING)) {
            return;
        }

        createNotification(new JSONObject(remoteMessage.getData()));
    }

    private void createNotification(JSONObject object) {


        try {
            String notificationTitle = "Title";
            String message = "Hello testing message";

            int id = 0;
            String TITLE = "title";
            if (object.has(TITLE)) {
                notificationTitle = object.getString(TITLE);
            }
            String DESCRIPTION = "description";
            if (object.has(DESCRIPTION)) {
                message = object.getString(DESCRIPTION);
            }
            String ID = "id";
            if (object.has(ID)) {
                id = object.getInt(ID);
            }


            Intent intent = new Intent(this, ResideMenuActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(Constants.TITLE, notificationTitle);

            intent.putExtras(bundle);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);//Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            //startActivity(intent);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                    intent, PendingIntent.FLAG_ONE_SHOT);

            Uri defaultSoundUri = RingtoneManager
                    .getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.drawable.ic_launcher)
                    .setContentTitle(notificationTitle)
                    .setContentText(message)
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent);

            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(id, notificationBuilder.build());

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }
}
package com.adc.origamistudios.presentation.account;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.adc.origamistudios.AppController;
import com.adc.origamistudios.R;
import com.adc.origamistudios.model.apiresponse.LoginResponse;
import com.adc.origamistudios.utilities.Log;
import com.adc.origamistudios.utilities.Constants;
import com.adc.origamistudios.utilities.StatusCode;
import com.adc.origamistudios.utilities.StatusMessage;
import com.adc.origamistudios.utilities.Util;
import com.adc.origamistudios.widgets.CustomButton;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SignUpActivity extends AppCompatActivity implements View.OnClickListener, Constants {

    private CustomButton btnCreateAccount, btnLogin;
    private String mFirstName, mLastName, mGender, mUserName, mEmail, mPassword, mConfirmPassword;
    private Dialog progressBar;
    private String deviceToken = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        initialize();
        initClickListeners();
        deviceToken = Util.getDeviceToken();
    }


    private void initialize() {
        btnCreateAccount = (CustomButton) findViewById(R.id.btnSignin);
        btnLogin = (CustomButton) findViewById(R.id.btnLogin);
        progressBar = Util.loadingDialog(this);
        Log.d("devicetoken", Util.getDeviceToken());
    }

    private void initClickListeners() {
        btnCreateAccount.setOnClickListener(this);
        btnLogin.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == btnCreateAccount) {
            if (validateFields()) {
                hitCreateAccountRequest();
            }
        } else if (v == btnLogin) {
            hitLoginRequest();
        }
    }


    private boolean validateFields() {

        mFirstName = "test";
        mLastName = "test";
        mUserName = "test12345678";
        mEmail = "test@test1.com";
        mPassword = "test12345678";
        mConfirmPassword = "test12345678";
        mGender = "Male";


        return true;
    }


    private void hitCreateAccountRequest() {
        if (!Util.isConnectingToInternet(this)) {
            Util.showToastMsg(this, Constants.kStringNetworkConnectivityError);
            return;
        }

        progressBar.show();
        if (null == deviceToken) {
            deviceToken = Util.getDeviceToken();
        } else if (deviceToken.equalsIgnoreCase("")) {
            deviceToken = Util.getDeviceToken();
        }
        if (deviceToken == null || deviceToken.isEmpty()) {
            deviceToken = "9qdjsahdksadasdsadasdadasdsadadasd";
        }
        Call<LoginResponse> call = AppController.getApiService().getRegistrationResponse(mFirstName, mLastName, mUserName, mPassword, mConfirmPassword, mGender, mEmail, mDeviceType, deviceToken);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                progressBar.dismiss();
                try {
                    if (response.body().getResponseCode() == StatusCode.SUCCESS) {
                        Util.showToastMsg(getApplicationContext(), response.body().getMessage());
                    } else {
                        Util.showToastMsg(getApplicationContext(), response.body().getMessage());
                    }
                } catch (Exception e) {
                    Util.showToastMsg(getApplicationContext(), StatusMessage.EXCEPTION_MESSAGE);
                    Log.e(Constants.EXCEPTION, e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                progressBar.dismiss();
                Log.e(Constants.EXCEPTION, t.getMessage());
                Util.showToastMsg(getApplicationContext(), StatusMessage.SERVER_EXCEPTION_MESSAGE);
            }
        });

    }


    private void hitLoginRequest() {
        if (!Util.isConnectingToInternet(this)) {
            Util.showToastMsg(this, Constants.kStringNetworkConnectivityError);
            return;
        }

        progressBar.show();
        if (null == deviceToken) {
            deviceToken = Util.getDeviceToken();
        } else if (deviceToken.equalsIgnoreCase("")) {
            deviceToken = Util.getDeviceToken();
        }

        Call<LoginResponse> call = AppController.getApiService().getLoginResponse("test12345678", "test12345678", Constants.mDeviceType, deviceToken);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                progressBar.dismiss();
                try {
                    if (response.body().getResponseCode() == StatusCode.SUCCESS) {
                        Util.showToastMsg(getApplicationContext(), response.body().getMessage());
                    } else {
                        Util.showToastMsg(getApplicationContext(), response.body().getMessage());
                    }
                } catch (Exception e) {
                    Util.showToastMsg(getApplicationContext(), StatusMessage.EXCEPTION_MESSAGE);
                    Log.e(Constants.EXCEPTION, e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                progressBar.dismiss();
                Log.e(Constants.EXCEPTION, t.getMessage());
                Util.showToastMsg(getApplicationContext(), StatusMessage.SERVER_EXCEPTION_MESSAGE);
            }
        });

    }

}

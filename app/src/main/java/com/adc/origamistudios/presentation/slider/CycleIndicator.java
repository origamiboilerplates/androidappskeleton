package com.adc.origamistudios.presentation.slider;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.adc.origamistudios.R;
import com.liangbx.android.banner.Indicator;
import com.liangbx.android.banner.model.BannerItem;
import com.rd.PageIndicatorView;

public class CycleIndicator implements Indicator {

    private final int mSize;
    private PageIndicatorView mIndicatorView;

    public CycleIndicator(int size) {
        mSize = size;
    }

    @Override
    public View getLayout(Context context, ViewGroup parent) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.banner_indicator, parent, false);
        mIndicatorView = view.findViewById(R.id.pageindicator);
        mIndicatorView.setCount(mSize);
        return view;
    }

    @Override
    public int getGravity() {
        return Gravity.BOTTOM | Gravity.CENTER;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels, BannerItem bannerItem) {
        mIndicatorView.onPageScrolled(position, positionOffset, positionOffsetPixels);
    }

    @Override
    public void onPageSelected(int position, BannerItem bannerItem) {
        mIndicatorView.onPageSelected(position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {
        mIndicatorView.onPageScrollStateChanged(state);
    }
}
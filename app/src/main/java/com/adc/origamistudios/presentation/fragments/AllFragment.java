package com.adc.origamistudios.presentation.fragments;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.adc.origamistudios.R;
import com.adc.origamistudios.presentation.activities.ResideMenuActivity;
import com.adc.origamistudios.utilities.Constants;
import com.adc.origamistudios.utilities.Util;

import static android.app.Activity.RESULT_OK;

public class AllFragment extends Fragment implements View.OnClickListener {
    private Toolbar toolbar;
    private Dialog progressBar;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        progressBar = Util.loadingDialog(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_all, container, false);
        toolbar = getActivity().findViewById(R.id.toolbar);
        initialize(view);
        return view;
    }

    private void initialize(View view) {

    }


    @Override
    public void onStop() {
        super.onStop();
    }


    @Override
    public void onClick(View v) {

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        //inflater.inflate(R.menu.menu_brands, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_search) {
        }
        return false;
    }


    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.action_search);
        item.setVisible(true);
    }

    private void launchFragment(Bundle bundle, Fragment fragment) {
        ((ResideMenuActivity) getActivity()).launchMFragment(bundle, fragment);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Constants.REQUEST_101 && resultCode == RESULT_OK) {

        }
    }

}


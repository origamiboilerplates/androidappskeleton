package com.adc.origamistudios.presentation.pager;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.adc.origamistudios.presentation.fragments.AllFragment;
import com.adc.origamistudios.presentation.fragments.Fragment2;
import com.adc.origamistudios.presentation.fragments.Fragment1;

public class HomePagerAdapter extends FragmentStatePagerAdapter {
    private final int mNumOfTabs;
    private final Context context;

    public HomePagerAdapter(Context context, FragmentManager fm, int numOfTabs) {
        super(fm);
        this.mNumOfTabs = numOfTabs;
        this.context = context;
    }

    @Override
    public Fragment getItem(int position) {

        Fragment fragment = null;
        switch (position) {
            case 0:
                fragment = new AllFragment();
                break;
            case 1:
                fragment = new Fragment1();
                break;
            case 2:
                fragment = new Fragment2();
                break;
            default:
                break;
        }
        return fragment;

    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}

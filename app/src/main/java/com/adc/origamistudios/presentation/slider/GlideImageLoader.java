package com.adc.origamistudios.presentation.slider;

import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.liangbx.android.banner.ImageLoader;

public class GlideImageLoader implements ImageLoader {

    @Override
    public void onLoad(ImageView imageView, String imageUrl) {
        RequestOptions imageLoaderOptions = new RequestOptions()
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.ALL);
              /* .placeholder(R.mipmap.defautl_image)
                .error(R.mipmap.defautl_image)*/
        Glide.with(imageView.getContext())
                .load(imageUrl)
                .apply(imageLoaderOptions)
                .into(imageView);
    }

    @Override
    public void onUnLoad(ImageView imageView) {

    }
}

package com.adc.origamistudios.presentation.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.adc.origamistudios.R;
import com.adc.origamistudios.presentation.activities.ResideMenuActivity;
import com.adc.origamistudios.presentation.pager.HomePagerAdapter;
import com.special.ResideMenu.ResideMenu;

public class HomeFragment extends Fragment implements TabLayout.OnTabSelectedListener, ViewPager.OnPageChangeListener {


    private ViewPager viewPager;
    private Toolbar toolbar;
    private int selected = 0;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        toolbar = getActivity().findViewById(R.id.toolbar);
        selected = getArguments().getInt("value");
        setTitle(selected);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View parentView = inflater.inflate(R.layout.home, container, false);
        setupViewPager(parentView, selected);
        //toolbar.setTitle(getResources().getString(R.string.nav_item_influencers));
        return parentView;
    }

    private void setupViewPager(View view, int selectedTab) {
        viewPager = view.findViewById(R.id.pager);
        TabLayout tabLayout = view.findViewById(R.id.tabs);
        tabLayout.addTab(tabLayout.newTab().setText(getResources().getString(R.string.nav_item_all)));
        tabLayout.addTab(tabLayout.newTab().setText(getResources().getString(R.string.nav_item_tab2)));
        tabLayout.addTab(tabLayout.newTab().setText("TAB 3"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        //tabLayout.setPadding(30,15,30,15);
        tabLayout.setTabMode(TabLayout.MODE_FIXED);
        HomePagerAdapter mAdapter = new HomePagerAdapter(getContext(), getChildFragmentManager(), tabLayout.getTabCount());
        viewPager.setOffscreenPageLimit(2);
        viewPager.setAdapter(mAdapter);
        viewPager.setOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(this);
        viewPager.addOnPageChangeListener(this);
        viewPager.setCurrentItem(selectedTab);
        mAdapter.notifyDataSetChanged();

    }

    private void setUpViews() {
        ResideMenuActivity parentActivity = (ResideMenuActivity) getActivity();
        ResideMenu resideMenu = parentActivity.getResideMenu();

        /*parentView.findViewById(R.id.btn_open_menu).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resideMenu.openMenu(ResideMenu.DIRECTION_LEFT);
            }
        });*/

        // add gesture operation's ignored views
        /*FrameLayout ignored_view = (FrameLayout) parentView.findViewById(R.id.ignored_view);
        resideMenu.addIgnoredView(ignored_view);*/
    }

    private void setTitle(int position) {
        if (position == 0) {
            toolbar.setTitle(getResources().getString(R.string.app_name));
        } else if (position == 1) {
            toolbar.setTitle(getResources().getString(R.string.nav_item_tab2));
        } else if (position == 2) {
            toolbar.setTitle("TAB 3");
        }
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        int position = tab.getPosition();
        viewPager.setCurrentItem(position);
        setTitle(position);
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}

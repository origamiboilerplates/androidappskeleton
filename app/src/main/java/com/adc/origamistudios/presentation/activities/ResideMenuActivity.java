package com.adc.origamistudios.presentation.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;

import com.adc.origamistudios.R;
import com.adc.origamistudios.presentation.fragments.HomeFragment;
import com.adc.origamistudios.utilities.Constants;
import com.adc.origamistudios.utilities.FontHelper;
import com.special.ResideMenu.ResideMenu;
import com.special.ResideMenu.ResideMenuItem;

public class ResideMenuActivity extends AppCompatActivity implements View.OnClickListener {

    private ResideMenu resideMenu;
    private Toolbar toolbar;
    View view;
    private String[] titles;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reside_menu);
        ResideMenuActivity mContext = this;
        initToolBar();
        initializeResideMenu();
        setUpMenu();
        setItemColor(1);
        Intent mIntent = getIntent();
        if (mIntent != null && mIntent.hasExtra(Constants.TITLE)) {
        } else {
            if (savedInstanceState == null)
                changeHomeFragment(new HomeFragment(), 0);
        }
    }

    private void initToolBar() {
        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(getString(R.string.app_name));
        toolbar.setNavigationIcon(R.drawable.ic_menu);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resideMenu.openMenu(ResideMenu.DIRECTION_LEFT);
            }
        });

        FontHelper.applyFontForToolbarTitle(this);
    }

    private void setItemColor(int item) {

        for (int i = 1; i < resideMenu.getMenuItems(0).size(); i++) {
            ResideMenuItem reItem = resideMenu.getMenuItems(0).get(i);
            if (i == item) {
                reItem.getTitle().setTextColor(getResources().getColor(R.color.faq_item_left));
            } else {
                reItem.getTitle().setTextColor(getResources().getColor(R.color.black));
            }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_search) {
        }
        return true;
    }

    private void initializeResideMenu() {
        titles = getResources().getStringArray(R.array.nav_drawer_labels);
        resideMenu = new ResideMenu(this);
        resideMenu.setBackground(R.drawable.background_white);
        resideMenu.attachToActivity(this);
        resideMenu.setMenuListener(menuListener);
        resideMenu.setSwipeDirectionDisable(ResideMenu.DIRECTION_RIGHT);
        resideMenu.setSwipeDirectionDisable(ResideMenu.DIRECTION_LEFT);
        //valid scale factor is between 0.0f and 1.0f. leftmenu'width is 150dip.
        resideMenu.setScaleValue(0.5f);
    }

    private void setUpMenu() {

        for (int i = 0; i < titles.length; i++) {
            final ResideMenuItem item = new ResideMenuItem(this);
            item.setTitle(titles[i]);
            item.setTag(titles[i]);
            item.setId(i);
            FontHelper.applyRegularFont(this, item.getTitle());

            resideMenu.addMenuItem(item, ResideMenu.DIRECTION_LEFT);
            if (i == 0) {
                setMargin(item, 0, 0, 0, 100);
                item.setIcon(R.drawable.ic_default_profile);
                item.setEmail("");
                item.getTitle().setTextSize(14);
                item.getTitle().setAllCaps(true);
                item.setOnClickListener(this);

            } else {
                setMargin(item, 0, 0, 0, 15);
                item.getEmail().setVisibility(View.GONE);
                item.getIcon().setVisibility(View.GONE);
                item.setOnClickListener(this);
            }
        }

       /*findViewById(R.id.nav_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resideMenu.openMenu(ResideMenu.DIRECTION_LEFT);
            }
        });*/
    }

    private void setMargin(ResideMenuItem item, int left, int top, int right, int bottom) {
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) item.getLayoutParams();
        params.gravity = Gravity.BOTTOM;
        params.setMargins(left, top, right, bottom);
        item.setLayoutParams(params);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        return resideMenu.dispatchTouchEvent(ev);
    }

    @Override
    public void onClick(View view) {
        String tag = (String) view.getTag();
        setItemColor(view.getId());

        if (tag.equals(getResources().getString(R.string.nav_item_home))) {
            changeHomeFragment(new HomeFragment(), 0);
        } else if (tag.equals(getResources().getString(R.string.nav_item_tab2))) {
            changeHomeFragment(new HomeFragment(), 1);
        }
        resideMenu.closeMenu();
    }

    private final ResideMenu.OnMenuListener menuListener = new ResideMenu.OnMenuListener() {
        @Override
        public void openMenu() {
        }

        @Override
        public void closeMenu() {
        }
    };

    private void changeFragment(Fragment targetFragment) {
        resideMenu.clearIgnoredViewList();
        launchAboutFragment(targetFragment);
    }

    private void changeHomeFragment(Fragment targetFragment, int tag) {
        Bundle bundle = new Bundle();
        bundle.putInt(Constants.FRAG_POSITION, tag);
        targetFragment.setArguments(bundle);
        resideMenu.clearIgnoredViewList();
        launchAboutFragment(bundle, targetFragment);
    }

    private void launchAboutFragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.main_fragment, fragment);
        //fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    private void launchAboutFragment(Bundle bundle, Fragment fragment) {
        fragment.setArguments(bundle);
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.main_fragment, fragment);
        //fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    public ResideMenu getResideMenu() {
        return resideMenu;
    }

    private void clearBackStack() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        if (fragmentManager.getBackStackEntryCount() > 0) {
            fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public void launchMFragment(Bundle bundle, Fragment fragment) {

        fragment.setArguments(bundle);
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.main_fragment, fragment);
        //fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    public void launchSimpleFragment(Bundle bundle, Fragment fragment) {
        fragment.setArguments(bundle);
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.main_fragment, fragment);
        //fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    private void goBack() {
        if (getFragmentManager().getBackStackEntryCount() > 0) {
            getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Log.e("New Intent", "Called");
    }

    @Override
    public void onBackPressed() {
        goBack();
    }
}
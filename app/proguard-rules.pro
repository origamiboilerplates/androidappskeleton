#-optimizations !code/simplification/arithmetic,!code/simplification/cast,!field/*,!class/merging/*,!class/unboxing/enum
#-optimizationpasses 1
-allowaccessmodification

-dontwarn com.google.android.gms.**

#Twilio
#-keep class com.twilio.** { *; }
#-keep class org.webrtc.** { *; }
#-keep class com.twilio.video.** { *; }
#-keepattributes InnerClasses

# DB Snappy and Kyro
-dontshrink
    -verbose
    -dontwarn sun.reflect.**
    -dontwarn java.beans.**
    -keep,allowshrinking class com.esotericsoftware.** {
       <fields>;
       <methods>;
    }
    -keep,allowshrinking class java.beans.** { *; }
    -keep,allowshrinking class sun.reflect.** { *; }
    -keep,allowshrinking class com.esotericsoftware.kryo.** { *; }
    -keep,allowshrinking class com.esotericsoftware.kryo.io.** { *; }
    -keep,allowshrinking class sun.nio.ch.** { *; }
    -dontwarn sun.nio.ch.**
    -dontwarn sun.misc.**
    -keep,allowshrinking class com.snappydb.** { *; }
    -dontwarn com.snappydb.**

# OkHTTP and Apache
-dontnote okhttp3.**, okio.**, retrofit2.**, pl.droidsonroids.**,retrofit.**
-dontwarn rx.internal.util.unsafe.**
-dontwarn javax.annotation.GuardedBy
-dontwarn javax.annotation.**
-dontwarn org.apache.**
-dontwarn org.apache.commons.collections.BeanMap

# For using GSON @Expose annotation
-keepattributes *Annotation*

# Gson specific classes
-keep class sun.* { *; }
-keep class com.google.gson.stream.** { *; }

# Remove Logs
#-assumenosideeffects class android.util.Log {
#    public static *** d(...);
#    public static *** v(...);
#    public static *** e(...);
#}

-keepclasseswithmembernames class * {
    native <methods>;
}

-keepclasseswithmembernames class * {
    public <init>(android.content.Context, android.util.AttributeSet);
}

-keepclasseswithmembernames class * {
    public <init>(android.content.Context, android.util.AttributeSet, int);
}

# Any constants
-keepclassmembers enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}

# Apache Specific Libraries
-keep class org.apache.http.**
-keep interface org.apache.http.**

# Keep retrofit
-dontwarn okio.**
-dontwarn retrofit.**
-keep class retrofit.** { *; }
-dontwarn retrofit2.**
-keep class retrofit2.** { *; }
-keepclasseswithmembers class * {
    @retrofit2.http.* <methods>;
    @retrofit.http.* <methods>;
}
-keepattributes Signature
-keepattributes Exceptions
-keepattributes InnerClasses

# Dont warn
# For Glide 4.3.1
-keep public class * implements com.bumptech.glide.module.GlideModule
-keep public class * extends com.bumptech.glide.module.AppGlideModule
-keep public enum com.bumptech.glide.load.resource.bitmap.ImageHeaderParser$** {
  **[] $VALUES;
  public *;
}